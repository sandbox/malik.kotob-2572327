<?php

/**
 * @file
 * File duplicates configuration.
 */

/**
 * Builds the file hash settings form.
 */
function file_duplicates_settings() {
  $form['file_duplicates_hash'] = array(
    '#default_value' => variable_get('file_duplicates_hash', 'sha256'),
    '#description' => t('The duplicate file listing will be based on the checked hash algorithm.'),
    '#options' => filehash_names(),
    '#title' => t('Hash algorithms'),
    '#type' => 'radios',
  );
  return system_settings_form($form);
}
