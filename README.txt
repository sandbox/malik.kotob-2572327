-- SUMMARY --

The File Duplicates module provides a list of a site's duplicate files
by leveraging the filehash module. The filehash module generates hashes
of a site's files via MD5, SHA-1, and/or SHA-256 algorithms.

Steps to view a listing of duplicate files:
1) Visit admin/config/media/filehash to select which algorithms you want
filehash to use to generate hashes of each file.
2) Visit admin/config/media/file-duplicates/ and select one of the algorithms
you selected in step 1.
3) Run 'drush generate-hashes' (alias - 'ghash')
4) Visit admin/config/media/file-duplicates/duplicates

For a full description of the module, visit the project page:
  http://drupal.org/project/

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/

-- REQUIREMENTS --

This module requires the filehash and media modules:
  http://drupal.org/project/filehash
  http://drupal.org/project/media


-- INSTALLATION --

Install as usual, see http://drupal.org/node/70151 for further instructions.
