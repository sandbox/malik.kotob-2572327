<?php
/**
 * @file
 * Drush command to load all files to generate hashes.
 */

/**
 * Implements hook_drush_command().
 */
function file_duplicates_drush_command() {
  $items['generate-hashes'] = array(
    'description' => 'Generate hashes of files in files_managed via filehash module by loading them.',
    'options' => array(
      'fid' => 'The fid',
    ),
    'aliases' => array('ghash'),
    'examples' => array(
      'drush generate-hashes' => 'Generates hashes of all files in files_managed.',
      'drush ghash' => 'Same as drush generate-hashes.',
      'drush ghash --fid=46' => 'Generates hash for file with a file id of 46.',
    ),
  );
  return $items;
}

/**
 * Callback for the generate-hashes command.
 */
function drush_file_duplicates_generate_hashes() {

  $fid = drush_get_option('fid', '');
  $message = '';

  if (!empty($fid)) {
    $result = db_query('SELECT fid FROM {file_managed} WHERE fid = :fid', array(':fid' => $fid));
    if (!empty($result)) {
      file_load($fid);
      $message = t("Hash has been generated for file @fid.", array('@fid' => $fid));
    }
  }
  else {
    // Query all files.
    $fids = db_query('SELECT fid FROM {file_managed}');

    // Load all files to generate hashes.
    foreach ($fids as $fid) {
      file_load($fid->fid);
    }
    $message = dt("Hashes have been generated for @number_loaded files. Visit admin/config/media/file-duplicates/duplicates for a list of duplicate files.", array('@number_loaded' => $fids->rowCount()));
  }

  drush_log($message, 'completed');

}
